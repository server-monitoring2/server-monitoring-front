import React , {Suspense}from 'react'
import {
    BrowserRouter as Router,
    Switch,
	Route,
} from "react-router-dom";

import './App.sass'
import { ToastContainer } from "react-toastify"
import Login from '../components/Login/Login'
import Register from '../components/Register/Register'
import Main from '../components/Main/Main'
import Pass from '../components/Pass/Pass'
import Reset from '../components/Reset/Reset'
import ActivateAccount from '../components/ActivateAccount/ActivateAccount'
import NotFound from '../components/NotFound/NotFound'
import TermOfRules from '../components/Documetation/TermOfRules'
import ChartTest from '../components/ChartTest/ChartTest'
import StripeTest from '../components/StripeTest/StripeTest'


class App extends React.Component {
	state = {
		token: null
	}

	
	setToken = value => {
		this.setState({token: value})
	}

	componentDidMount() {
		const token = localStorage.getItem('token')
		if(token){
			this.setToken(token)
		}
		
	}

	render () 
	{
	  return (
		<Suspense fallback="loading">
			<Router>
				<Switch>
					<Route path='/' exact component={() => <Main token={this.state.token} setToken={this.setToken}/> } /> 
					<Route path='/login' component={()=><Login token={this.state.token} setToken={this.setToken}/>} /> 
	  				<Route path='/register' component={()=><Register token={this.state.token} setToken={this.setToken}/> }/>
					<Route path='/pass' component={()=><Pass token={this.state.token} setToken={this.setToken}/>} /> 
					<Route path='/reset/:reset_token' component={Reset} /> 
					<Route path='/documentation' component={TermOfRules} /> 
					<Route path='/activate/:token' component={ActivateAccount} /> 
					<Route path='/stripe' component={StripeTest} /> 
					<Route path='/test' component={ChartTest} /> 
					<Route component={()=><NotFound exact/>} /> 
				</Switch>
				
			</Router>
			<ToastContainer/>
			</Suspense>
			
	    );
	}
}

export default App