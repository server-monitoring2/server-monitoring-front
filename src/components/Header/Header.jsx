import React from "react"
import { API_ENDPOINT } from "../../api"
import "./Header.sass"
import ChangePass from "../ChangePass/ChangePass"
import AccountHover from "../ConfirmHover/AccountHover"
import { withRouter } from "react-router-dom"
import { toast } from "react-toastify"
import { withTranslation } from "react-i18next"

class Header extends React.Component {
    state = {
        publickey: "",
        privatekey: "",
        showChangePassword: false,
        errorStatus: 0,
        confDelete: false,
        username: "",
        status: false,
        statusInfo: "",
        uploadedPhoto: null,
        photo: " ",
        currentPhoto: " ",
        hiddenButton: true,
        avaAlert: "",
        isAvaAlert: true,
        statusColor: "",
        premiumShow : true
    }


    componentDidMount() {

        let a = ""
        const {
            i18n: { language },
        } = this.props
        fetch(`${API_ENDPOINT}/user/info`, {
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                } else {
                    this.setState({
                        username: res.username,
                        status: res.is_premium,
                    })
                    if (res.is_premium === "true") {
                        this.setState({
                            statusInfo: "prem",
                            statusColor: "#38dbff",
                            premiumShow : true,
                            
                        })
                        this.props.setRange(7)
                    } else {
                        this.setState({
                            statusInfo: "comm",
                            statusColor: "#495f0d",
                            premiumShow : false,
                        })
                        this.props.setRange(3)
                    }

                    a = res.username

                    fetch(`${API_ENDPOINT}/user/pic/get`, {
                        headers: {
                            Authorization: this.props.token,
                            "Accept-Language": language,
                        },
                    })
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                this.setState({
                                    photo: "",
                                    currentPhoto: this.createImageFromInitials(
                                        4000,
                                        a,
                                        this.getRandomColor()
                                    ),
                                })
                            } else {
                                this.setState({
                                    photo: "https://" + res.pic,
                                    currentPhoto: "https://" + res.pic,
                                })
                            }
                        })
                }
            })
    }

    fileSelectedHadler = event => {
        let fileItem = event.target.files[0]
        if (fileItem) {
            const newUrl = URL.createObjectURL(fileItem)
            this.setState({
                uploadedPhoto: fileItem,
                photo: newUrl.toString(),
                hiddenButton: false,
            })
        }
        event.target.value = null
    }

    fileDeleteHandler = () => {
        this.setState({
            isAvaAlert: true,
        })
        if (this.state.uploadedPhoto) {
            this.setState({
                uploadedPhoto: null,
                photo: this.state.currentPhoto,
                hiddenButton: true,
            })
        }
    }

    fileUploadHandler = () => {
        const {
            i18n: { language },
        } = this.props
        this.setState({
            isAvaAlert: true,
        })
        const fd = new FormData()
        fd.append("photo", this.state.uploadedPhoto)
        fetch(`${API_ENDPOINT}/user/pic/upload`, {
            method: "POST",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
            body: fd,
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    this.notify(res.error)
                } else {
                    this.notifyS()
                    fetch(`${API_ENDPOINT}/user/pic/get`, {
                        headers: {
                            Authorization: this.props.token,
                            "Accept-Language": language,
                        },
                    })
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                this.setState({
                                    photo: "",
                                    currentPhoto: this.createImageFromInitials(
                                        4000,
                                        this.state.username,
                                        this.getRandomColor()
                                    ),
                                })
                            } else {
                                this.setState({
                                    photo: "https://" + res.pic,
                                    currentPhoto: "https://" + res.pic,
                                    hiddenButton: true,
                                })
                            }
                        })
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }
    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("header.Uploaded"), {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    getRandomColor = () => {
        var letters = "0123456789ABCDEF"
        var color = "#"
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)]
        }
        return color
    }

    getInitials = name => {
        let initials
        const nameSplit = name.split(" ")
        const nameLength = nameSplit.length
        if (nameLength > 1) {
            initials =
                nameSplit[0].substring(0, 1) +
                nameSplit[nameLength - 1].substring(0, 1)
        } else if (nameLength === 1) {
            initials = nameSplit[0].substring(0, 1)
        } else return

        return initials.toUpperCase()
    }

    createImageFromInitials = (size, name, color) => {
        if (name == null) return
        name = this.getInitials(name)

        const canvas = document.createElement("canvas")
        const context = canvas.getContext("2d")
        canvas.width = canvas.height = size

        context.fillStyle = "#ffffff"
        context.fillRect(0, 0, size, size)

        context.fillStyle = `${color}50`
        context.fillRect(0, 0, size, size)

        context.fillStyle = color
        context.textBaseline = "middle"
        context.textAlign = "center"
        context.font = `${size / 2}px Roboto`
        context.fillText(name, size / 2, size / 2)

        return canvas.toDataURL()
    }

    render() {
        const { t } = this.props
        var status = ""
        if(this.state.statusInfo === "prem"){
            status = t("header.Premium")
        }
        else {
            status = t("header.Common")
        }
        return (
            <div className='user'>
                <div className='user-info'>
                    <label htmlFor='fileCreate' className='profile-modal-photo'>
                        <span
                            class='notify-badge'
                            style={{
                                background: this.state.statusColor,
                            }}>
                            {status}
                        </span>
                        <img
                            style={{
                                height: "",
                                width: "",
                            }}
                            src={
                                this.state.photo.length === 0
                                    ? this.state.currentPhoto
                                    : this.state.photo
                            }
                            alt='UserPhoto'
                        />

                        <input
                            id='fileCreate'
                            type='file'
                            accept='image/jpg, image/jpeg, image/png'
                            style={{ display: "none" }}
                            onChange={this.fileSelectedHadler}
                        />
                    </label>
                    <div classname="buttons">
                    <button
                        hidden={this.state.hiddenButton}
                        onClick={this.fileUploadHandler}>
                        {t("header.Upload")}
                    </button>
                    <button
                        hidden={this.state.hiddenButton}
                        onClick={this.fileDeleteHandler}>
                        {t("header.Cancel")}
                    </button>
                    </div>
                    <p className='user-info-text'>{this.state.username}</p>
                </div>
                <ul>
                    <li>
                        <ChangePass token={this.props.token} />
                    </li>

                    <li>
                        <button
                            onClick={() => {
                                this.props.setToken(null)
                                localStorage.removeItem('token')
                                
                            }}>
                            {t("header.Logout")}
                        </button>
                    </li>
                    <li>
                        <AccountHover
                            token={this.props.token}
                            setToken={this.props.setToken}
                        />
                    </li>
                    <li>
                        <button
                            hidden={this.state.premiumShow}
                            onClick={() => {
                                this.props.history.push("/stripe")
                            }}>
                            {t("header.Become premuim")}
                        </button>
                    </li>
                </ul>
            </div>
        )
    }
}

export default withTranslation()(withRouter(Header))
