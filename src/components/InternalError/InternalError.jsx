import React from "react"
import "./css/custom_50x.css"
import space from "./img/pepega.png"
import "./InternalError.sass"

class InternalError extends React.Component {

    render() {
        document.title = "Oopps..."
        return (
            <div className='InternalError'>
                <div className='left'>
                    <img src={space} alt='Error' />
                </div>
                <div class='header'>
                    <h1>
                    Ooops, something went terribly wrong!
                    </h1>
                    <p>
                    An unexpected error occurred on the server, please wait for it to be fixed.
                    </p>
                </div>
            </div>
        )
    }
}

export default InternalError
