import React from "react"
import { toast } from "react-toastify"
import "./Pass.sass"
import { Link, Redirect,withRouter} from "react-router-dom"
import { API_ENDPOINT } from "../../api"
import Footer from "../Footer/Footer"
import { withTranslation } from "react-i18next"
import VisualConfig from "../VisualConfig/VisualConfig"

class Pass extends React.Component {
    state = {
        username: "",
        email: "",
        cAlert: "",
        showAlert: true,
        showSuccess: true,
        errorStatus: 0,
        errorUsername: "",
        errorEmail: "",
        isValidUsername: true,
        isValidEmail: true,
    }

    forg = () => {
        const {
            i18n: { language }, history
        } = this.props
        this.setState({
            showAlert: true,
            showSuccess: true,
        })
        fetch(`${API_ENDPOINT}/password/reset`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept-Language": language,
            },
            body: JSON.stringify({
                username: this.state.username,
                email: this.state.email,
            }),
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    this.notify(res.error)
                } else {
                    history.push("/login")
                    this.notifyS()
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t} = this.props
        toast.dismiss()
        toast.success(t("pass.Check your mail!"), {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }



    handleValidation = (username, email) => {
        var a = false
        var b = false

        if (username.toString().length > 0) {
            this.setState({
                isValidUsername: true,
                errorUsername: "",
            })
            a = true
        } else {
            this.setState({
                isValidUsername: false,
                errorUsername: "input-error",
            })
        }

        if (
            /^[a-zA-Z0-9\d@#^$!%*?&._]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(
                email.toString()
            )
        ) {
            this.setState({
                isValidEmail: true,
                errorEmail: "",
            })
            b = true
        } else {
            this.setState({
                isValidEmail: false,
                errorEmail: "input-error",
            })
        }

        if (a && b) {
            this.forg()
        }
    }


    render() {
        toast.dismiss()
        const {t} = this.props
        document.title = t("pass.title")
        if (this.props.token) {
            return <Redirect to='/' exact />
        }
        return (
            <div className='pass-wrapper'>
                <div className='pass-bg'>
                    <h1>Server Monitoring Service</h1>
                </div>
                <div className='pass-content'>
                    <div className='pass-form'>
                        <h2>{t("pass.title")}</h2>
                        <div className='input-field'>
                            <label htmlFor='username' className='active'>
                                {t("pass.Username")}
                            </label>
                            <input
                                placeholder={t("pass.Username")}
                                id='username'
                                type='text'
                                className={
                                    "validate " + this.state.errorUsername
                                }
                                value={this.state.username}
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidUsername: false,
                                            errorUsername: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidUsername: true,

                                            errorUsername: "",
                                        })
                                    }
                                }}
                                onChange={e => {
                                    this.setState({ username: e.target.value })
                                }}
                                onFocus={() => {
                                    if (this.state.errorUsername !== "") {
                                        this.setState({
                                            errorUsername: "",
                                            isValidUsername: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.email
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidUsername ? null : (
                                <p className='front'>{t("pass.Login can't be empty")}</p>
                            )}
                        </div>
                        <div className='input-field'>
                            <label htmlFor='email' className='active'>
                                {t("pass.Email")}
                            </label>
                            <input
                                placeholder={t("pass.Email")}
                                id='email'
                                type='email'
                                className={"validate " + this.state.errorEmail}
                                value={this.state.email}
                                onBlur={e => {
                                    if (
                                        /^[a-zA-Z0-9\d@#^$!%*?&._]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidEmail: true,
                                            errorEmail: "",
                                        })
                                    } else {
                                        this.setState({
                                            isValidEmail: false,
                                            errorEmail: "input-error",
                                        })
                                    }
                                }}
                                onChange={e => {
                                    this.setState({ email: e.target.value })
                                }}
                                onFocus={() => {
                                    if (this.state.errorEmail !== "") {
                                        this.setState({
                                            errorEmail: "",
                                            isValidEmail: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.email
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidEmail ? null : (
                                <p className='front'>{t("pass.Enter email")}</p>
                            )}
                        </div>
                        <div className='form-btns'>
                            <button
                                disabled={
                                    this.state.username === "" ||
                                    this.state.email === "" ||
                                    this.state.isValidUsername === false ||
                                    this.state.isValidEmail === false
                                }
                                onClick={this.forg}>
                                {t("pass.Confirm")}
                            </button>
                        </div>
                        <div className='form-btns'>
                            <Link to='/login'>{t("pass.Back to login")}</Link>
                        </div>
                        <div className='form-btns'>
                            <Link to='/register'>{t("pass.Register")}</Link>
                        </div>
                    </div>
                </div>
                <VisualConfig />
                <Footer />
            </div>
        )
    }
}

export default withTranslation()(withRouter(Pass))
