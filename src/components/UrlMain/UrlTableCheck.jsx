import React from "react"
import { withTranslation } from "react-i18next"
import "./UrlTableCheck.sass"

class UrlTableCheck extends React.Component {
    state = {
        checkRate: "",
        checkSize: "",
        lastCheck: "",
        lastCode: "",
        lastInfo: "",
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.getUrlData()
        }
    }

    getUrlData = () => {
        const { t } = this.props
        if (!this.props.agent.error || this.props.agent.length !== 0) {
            let a = +this.props.agent.check_rate
            let b = ""
            let c = +this.props.agent.last_code
            let d = +this.props.agent.check_size
            if (c >= 400 || c === 0) {
                b = t("urlTableChart.false")
            } else {
                b = t("urlTableChart.true")
            }
            if (c === 0) {
                console.log(123)
                this.setState({
                    lastCode: "Failed",
                })
            } else {
                this.setState({
                    lastCode: this.props.agent.last_code,
                })
            }
            a *= 100
            this.setState({
                checkRate: a.toFixed(2),
                checkSize: d+1,
                lastCheck: this.props.agent.last_check,
                lastInfo: b,
            })
        }
    }
    render() {
        const { t } = this.props
        if (this.props.agent.error || this.props.agent.length === 0) {
            return <p>{t("urlTableChart.Nothing to show yet")}</p>
        }
        return (
            <div className='box'>
                <p>
                    {t("urlTableChart.Available for last check")} :{" "}
                    {this.state.lastInfo}
                </p>
                <p>
                    {t("urlTableChart.Last check status")} :{" "}
                    {this.state.lastCode}
                </p>
                <p>
                    {t("urlTableChart.Time of last check")} :{" "}
                    {this.state.lastCheck}
                </p>
                <p>
                    {t("urlTableChart.Total checks")} : {this.state.checkSize}
                </p>
                <p>
                    {t("urlTableChart.Percentage of valid checks")}:{" "}
                    {this.state.checkRate}%
                </p>
            </div>
        )
    }
}

export default withTranslation()(UrlTableCheck)
