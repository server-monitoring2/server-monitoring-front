import React from "react"
import UrlTable from "../UrlMain/UrlTable"
import UrlTableCheck from "../UrlMain/UrlTableCheck"
import "./UrlMain.sass"

class UrlMain extends React.Component {
    state = {
        selectedServerName: [],
    }
    setSeverName = value => {
        this.setState({ selectedServerName: value })
    }

    render() {
        return (
            <div className='agent-header-main'>
                <div className='agent-header-select'>
                    <UrlTable
                        setData={this.setSeverName}
                        token={this.props.token}
                        setToken={this.props.setToken}
                    />
                </div>
                <div className='agent-header-table'>
                    <UrlTableCheck
                        agent={this.state.selectedServerName}
                        setData={this.setSeverName}
                    />
                </div>
            </div>
        )
    }
}

export default UrlMain
