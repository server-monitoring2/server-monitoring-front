import React from "react"
import { API_ENDPOINT } from "../../api"
import Select from "react-select"
import "./UrlTable.sass"
import AddUrl from "../AddUrl/AddUrl"
import { withTranslation } from "react-i18next"
import UrlHover from "../ConfirmHover/UrlHover"

class UrlTable extends React.Component {
    state = {
        agentData: [],
        serverNames: [],
        name: "",
        nameVisible: true,
        showAddServer: false,
        showChangeServer: false,
        showSuccess: true,
        selectedServerName: "",
        isLoaded: true,
        selectValue: null,
        timer: null,
    }

    componentDidMount() {
        const {
            i18n: { language },
        } = this.props
        fetch(`${API_ENDPOINT}/sites/names`, {
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    if (res.error === "Invalid token") {
                        this.props.setToken(null)
                    }
                } else {
                    console.log(res)
                    this.setState({
                        serverNames: res.map(obj => ({
                            label: obj.url,
                            value: obj.url,
                        })),
                    })
                }
            })
    }

    setServerName = servName => {
        this.setState({
            serverNames: servName.map(obj => ({
                label: obj.url,
                value: obj.url,
            })),
        })
    }

    componentWillUnmount() {
        clearInterval(this.state.timer)
    }
    stopTimer = () =>{
        if (this.state.timer !== null) {
            clearInterval(this.state.timer)
        }
        this.props.setData([])
    }
    doRequest = id => {
        const {
            i18n: { language },
        } = this.props
        this.setState({
            isLoaded: false,
        })
        console.log(id)

        fetch(`${API_ENDPOINT}/user/site/list/${id}`, {
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    isLoaded: true,
                })

                if (res.error) {
                    this.setState({
                        agentData: res,
                        nameVisible: false,
                    })
                } else {
                    this.setState({
                        agentData: res,
                        nameVisible: false,
                    })
                }
                this.props.setData(res)
            })
        if (this.state.timer !== null) {
            clearInterval(this.state.timer)
        }
        this.setState({
            timer: setInterval(() => {
                const {
                    i18n: { language },
                } = this.props

                fetch(`${API_ENDPOINT}/user/site/list/${id}`, {
                    headers: {
                        Authorization: this.props.token,
                        "Accept-Language": language,
                    },
                })
                    .then(res => res.json())
                    .then(res => {
                        this.setState({
                            isLoaded: true,
                        })

                        if (res.error) {
                            this.setState({
                                agentData: res,
                                nameVisible: false,
                            })
                        } else {
                            this.setState({
                                agentData: res,
                                nameVisible: false,
                            })
                        }
                        this.props.setData(res)
                    })
            }, 70000),
        })
    }

    render() {
        const { t } = this.props
        return (
            <div className='data-main'>
                <div className='data-button'>
                    <ul>
                        <li>
                            <AddUrl
                                token={this.props.token}
                                setServerName={this.setServerName}
                            />
                        </li>
                        <li>
                            <UrlHover
                                setSelectedValue={() => {
                                    this.setState({
                                        selectValue: null,
                                    })
                                }}
                                setSelectedServer={() => {
                                    this.setState({
                                        saveSelected: null,
                                    })
                                }}
                                token={this.props.token}
                                setServerName={this.setServerName}
                                saveSelected={this.state.saveSelected}
                                setVisible={() => {
                                    this.setState({
                                        nameVisible: true,
                                    })
                                }}
                                stopTimer = {this.stopTimer}
                            />
                        </li>
                    </ul>
                </div>
                <div className='data-info-url'>
                    <Select
                        options={this.state.serverNames}
                        className='select-component'
                        value={this.state.selectValue}
                        onChange={e => {
                            if (e.value !== this.state.saveSelected) {
                                this.setState({
                                    selectValue: e,
                                    name: e.value,
                                    saveSelected: e.value,
                                })
                                this.doRequest(e.value)
                            }
                        }}
                    />

                    {this.state.isLoaded ? null : (
                        <div className='loading'>
                            <div class='lds-ellipsis'>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    )}
                    {this.state.nameVisible ? null : (
                        <p className='agent'>
                            {t("urlTable.Url")}: {this.state.name}
                        </p>
                    )}
                </div>
            </div>
        )
    }
}

export default withTranslation()(UrlTable)
