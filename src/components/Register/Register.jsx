import React from "react"
import { toast } from "react-toastify"
import "./Register.sass"
import { withTranslation } from "react-i18next"
import { Link, Redirect,withRouter} from "react-router-dom"
import { API_ENDPOINT } from "../../api"
import NotFound from "../NotFound/NotFound"
import InternalError from "../InternalError/InternalError"
import Footer from "../Footer/Footer"
import VisualConfig from "../VisualConfig/VisualConfig"

class Register extends React.Component {
    state = {
        username: "",
        password: "",
        email: "",
        cAlert: "",
        showAlert: true,
        showSuccess: true,
        isValidUsername: true,
        isValidPassword: true,
        isValidEmail: true,
        errorUsername: "",
        errorPass: "",
        errorEmail: "",
        errorStatus: 0,
    }

    handleReg = () => {
        this.setState({
            showAlert: true,
            showSuccess: true,
        })
        const { i18n:{language}, history} = this.props
        fetch(`${API_ENDPOINT}/register`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept-Language" : language
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                email: this.state.email,
            }),
        })
            .then(res => {
                if (res.status === 404) {
                    this.setState({ errorStatus: 404 })
                }
                if (res.status === 502) {
                    this.setState({ errorStatus: 502 })
                }
                return res.json()
            })
            .then(res => {
                if (res.ok === true) {
                    
                    history.push("/login")
                    this.notifyS()
                    
                } else {
                    this.notify(res.error)
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t} = this.props
        toast.dismiss()
        toast.success(t("register.Registration success, check mail"), {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }


    handleValidation = (username, password, email) => {
        var a = false
        var b = false
        var c = false

        if (/^[A-Za-z\d@#^$!%*?&_.]{2,20}$/.test(username.toString())) {
            this.setState({
                isValidUsername: true,
                errorUsername: "",
            })
            a = true
        } else {
            this.setState({
                isValidUsername: false,
                errorUsername: "input-error",
            })
        }

        if (
            /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@#^$!%*?&._]{8,}$/.test(
                password.toString()
            )
        ) {
            this.setState({
                isValidPassword: true,
                errorPass: "",
            })
            b = true
        } else {
            this.setState({
                isValidPassword: false,
                errorPass: "input-error",
            })
        }

        if (
            /^[a-zA-Z0-9\d@#^$!%*?&._]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(
                email.toString()
            )
        ) {
            this.setState({
                isValidEmail: true,
                errorEmail: "",
            })
            c = true
        } else {
            this.setState({
                isValidEmail: false,
                errorEmail: "input-error",
            })
        }
        if (a && b && c) {
            this.handleReg()
        }
    }


    render() {
        toast.dismiss()
        const { t} = this.props
        document.title = t("register.title")
        if (this.props.token) {
            return <Redirect to='/' exact />
        }
        if (this.state.errorStatus === 404) {
            return <NotFound />
        }
        if (this.state.errorStatus === 502) {
            return <InternalError />
        }
        return (
            <div className='reg-wrapper'>
                <div className='reg-bg'>
                    <h1>Server Monitoring Service</h1>
                </div>
                <div className='reg-content'>
                    <div className='reg-form'>
                        <h2>{t("register.title")}</h2>
                        <div className='input-field'>
                            <label htmlFor='username' className='active'>
                                {t("register.Username")}
                            </label>
                            <input
                                placeholder={t("register.Username")}
                                id='username'
                                type='text'
                                className={
                                    "validate " + this.state.errorUsername
                                }
                                value={this.state.username}
                                onBlur={e => {
                                    if (
                                        /^[A-Za-z\d@#^$!%*?&_.]{2,20}$/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidUsername: true,
                                            errorUsername: "",
                                        })
                                    } else {
                                        this.setState({
                                            isValidUsername: false,
                                            errorUsername: "input-error",
                                        })
                                    }
                                }}
                                onChange={e =>
                                    this.setState({ username: e.target.value })
                                }
                                onFocus={() => {
                                    if (this.state.errorUsername !== "") {
                                        this.setState({
                                            errorUsername: "",
                                            isValidUsername: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.password,
                                            this.state.email
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidUsername ? null : (
                                <p>
                                    {t("register.ValidateUsername")}
                                </p>
                            )}
                        </div>
                        <div className='input-field'>
                            <label htmlFor='pass' className='active'>
                                {t("register.Password")}
                            </label>
                            <input
                                placeholder= {t("register.Password")}
                                id='pass'
                                type='password'
                                className={"validate " + this.state.errorPass}
                                value={this.state.password}
                                onBlur={e => {
                                    if (
                                        /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@#^$!%*?&._]{8,}$/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidPassword: true,
                                            errorPass: "",
                                        })
                                    } else {
                                        this.setState({
                                            isValidPassword: false,
                                            errorPass: "input-error",
                                        })
                                    }
                                }}
                                onChange={e => {
                                    this.setState({ password: e.target.value })
                                }}
                                onFocus={() => {
                                    if (this.state.errorPass !== "") {
                                        this.setState({
                                            errorPass: "",
                                            isValidPassword: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.password,
                                            this.state.email
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidPassword ? null : (
                                <p className='front'>
                                    {t("register.ValidatePass")}
                                </p>
                            )}
                        </div>
                        <div className='input-field'>
                            <label htmlFor='email' className='active'>
                                {t("register.Email")}
                            </label>
                            <input
                                placeholder= {t("register.Email")}
                                id='email'
                                type='email'
                                className={"validate " + this.state.errorEmail}
                                value={this.state.email}
                                onBlur={e => {
                                    if (
                                        /^[a-zA-Z0-9\d@#^$!%*?&._]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidEmail: true,
                                            errorEmail: "",
                                        })
                                    } else {
                                        this.setState({
                                            isValidEmail: false,
                                            errorEmail: "input-error",
                                        })
                                    }
                                }}
                                onChange={e => {
                                    this.setState({ email: e.target.value })
                                }}
                                onFocus={() => {
                                    if (this.state.errorEmail !== "") {
                                        this.setState({
                                            errorEmail: "",
                                            isValidEmail: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.password,
                                            this.state.email
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidEmail ? null : (
                                <p className='front'>{t("register.Enter email")}</p>
                            )}
                        </div>
                        <div className='form-btns'>
                            <button
                                disabled={
                                    this.state.username === "" ||
                                    this.state.password === "" ||
                                    this.state.email === "" ||
                                    this.state.isValidUsername === false ||
                                    this.state.isValidPassword === false ||
                                    this.state.isValidEmail === false
                                }
                                onClick={this.handleReg}>
                                {t("register.Register")}
                            </button>
                        </div>
                        <div className='form-btns'>
                            <Link to='/login'>{t("register.Back to login")}</Link>
                        </div>
                        <div className='form-btns'>
                            <Link to='/pass'>{t("register.Forgot password")}</Link>
                        </div>
                    </div>
                </div>
                <VisualConfig/>
                <Footer />
            </div>
        )
    }
}

export default withTranslation()(withRouter(Register))
