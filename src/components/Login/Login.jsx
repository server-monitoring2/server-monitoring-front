import React from "react"

import "./Login.sass"
import { toast } from "react-toastify"
import { Link, Redirect } from "react-router-dom"
import { API_ENDPOINT } from "../../api"
import Footer from "../Footer/Footer"
import NotFound from "../NotFound/NotFound"
import InternalError from "../InternalError/InternalError"
import VisualConfig from "../VisualConfig/VisualConfig"

import { withTranslation } from "react-i18next"

class Login extends React.Component {
    state = {
        username: "",
        password: "",
        cAlert: "",
        showAlert: true,
        isValidUsername: true,
        isValidPassword: true,
        errorUsername: "",
        errorPass: "",
        showChangePassMessage: false,
        errorStatus: 0,
    }

    handleLogin = () => {
        const {
            i18n: { language }
        } = this.props
        fetch(`${API_ENDPOINT}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept-Language": language,
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            }),
        })
            .then(res => {
                if (res.status === 404) {
                    this.setState({ errorStatus: 404 })
                }
                if (res.status === 502) {
                    this.setState({ errorStatus: 502 })
                }
                return res.json()
            })
            .then(res => {
                if (res.token) {
                    localStorage.setItem("token", res.token)
                    this.props.setToken(res.token)
                } else {
                    this.notify(res.error)
                    
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    handleValidation = (username, password) => {
        var a = false
        var b = false

        if (username.toString().length > 0) {
            this.setState({
                isValidUsername: true,
                errorUsername: "",
            })
            a = true
        } else {
            this.setState({
                isValidUsername: false,
                errorUsername: "input-error",
            })
        }

        if (password.toString().length > 0) {
            this.setState({
                isValidPassword: true,
                errorPass: "",
            })
            b = true
        } else {
            this.setState({
                isValidPassword: false,
                errorPass: "input-error",
            })
        }

        

        if (a && b) {
            this.handleLogin()
        }
    }


    render() {
        toast.dismiss()
        const { t } = this.props
        document.title = t("login.title")
        if (this.props.token) {
            return <Redirect to='/' exact />
        }
        if (this.state.errorStatus === 404) {
            return <NotFound />
        }
        if (this.state.errorStatus === 502) {
            return <InternalError />
        }
        return (
            <div className='login-wrapper'>
                <div className='login-bg'>
                    <h1>Server Monitoring Service</h1>
                </div>

                <div className='login-content'>
                    <div className='login-form'>
                        <h2>{t("login.title")}</h2>
                        <div className='input-field'>
                            <label htmlFor='username' className='active'>
                                {t("login.Username")}
                            </label>
                            <input
                                placeholder={t("login.Username")}
                                id='username'
                                type='text'
                                className={
                                    "validate " + this.state.errorUsername
                                }
                                value={this.state.username}
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidUsername: false,
                                            errorUsername: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidUsername: true,

                                            errorUsername: "",
                                        })
                                    }
                                }}
                                onChange={e => {
                                    this.setState({ username: e.target.value })
                                }}
                                onFocus={() => {
                                    if (this.state.errorUsername !== "") {
                                        this.setState({
                                            errorUsername: "",
                                            isValidUsername: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.password
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidUsername ? null : (
                                <p className='front'>{t("login.Login can't be empty")}</p>
                            )}
                        </div>

                        <div className='input-field'>
                            <label htmlFor='pass' className='active'>
                                {t("login.Password")}
                            </label>
                            <input
                                placeholder={t("login.Password")}
                                id='pass'
                                type='password'
                                className={"validate " + this.state.errorPass}
                                value={this.state.password}
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidPassword: false,
                                            errorPass: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidPassword: true,
                                            errorPass: "",
                                        })
                                    }
                                }}
                                onChange={e => {
                                    this.setState({ password: e.target.value })
                                }}
                                onFocus={() => {
                                    if (this.state.errorPass !== "") {
                                        this.setState({
                                            errorPass: "",
                                            isValidPassword: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.username,
                                            this.state.password
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidPassword ? null : (
                                <p className='front'>{t("login.Password can't be empty")}</p>
                            )}
                        </div>
                        <div className='form-btns'>
                            <button
                                disabled={
                                    this.state.username === "" ||
                                    this.state.password === "" ||
                                    this.state.isValidUsername === false ||
                                    this.state.isValidPassword === false
                                }
                                onClick={this.handleLogin}>
                                {t("login.Enter")}
                            </button>
                        </div>
                        <div className='form-btns'>
                            <Link to='/register'>{t("login.Register")}</Link>
                        </div>
                        <div className='form-btns'>
                            <Link to='/pass'>{t("login.Forgot password")}</Link>
                        </div>
                        
                    </div>
                </div>
                <VisualConfig />

                <Footer />
            </div>
        )
    }
}

export default withTranslation()(Login)
