import React from "react"
import { withRouter } from "react-router-dom"
import { API_ENDPOINT } from "../../api"
import { withTranslation } from "react-i18next"
import { toast } from "react-toastify"
import "./Reset.sass"

class Reset extends React.Component {
    state = {
        ready: false,
    }

    componentDidMount() {
        const {
            i18n: { language },
            history,
        } = this.props
        fetch(
            `${API_ENDPOINT}/password/reset/${this.props.match.params.reset_token}`,
            {
                method: "PUT",
                headers: {
                    "Accept-Language": language,
                },
            }
        )
            .then(res => res.json())
            .then(res => {
                if (res.ok === true) {
                    history.push("/login")
                    this.notifyS()
                } else if (res.error) {
                    history.push("/login")
                    this.notify(res.error)
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("reset.Check your mail for new pass!"), {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    render() {
        const { t } = this.props
        document.title = t("reset.title")
        return null
    }
}

export default withTranslation()(withRouter(Reset))
