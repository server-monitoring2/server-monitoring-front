import React from "react"
import { API_ENDPOINT } from "../../api"
import { toast } from "react-toastify"
import { withTranslation } from "react-i18next"
import "./DeleteHover.sass"
class DeleteHover extends React.Component {
    state = {
        showSuccess: true,
        showDelete: false,
    }

    deleteServer = () => {
        const {
            i18n: { language },
        } = this.props
        this.setState({
            showSuccess: true,
        })

        fetch(`${API_ENDPOINT}/user/server/delete/${this.props.saveSelected}`, {
            method: "DELETE",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    this.notify(res.error)
                } else {
                    this.notifyS()
                    fetch(`${API_ENDPOINT}/server/names`, {
                        headers: {
                            Authorization: this.props.token,
                            "Accept-Language": language,
                        },
                    })
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                if (res.error === "Invalid token") {
                                    this.props.setToken(null)
                                }
                            } else {
                                this.props.stopTimer()
                                this.props.setVisible()
                                this.props.setServerName(res)
                                this.props.setSelectedValue()
                                this.props.setSelectedServer()
                            }
                        })
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("deleteHover.Server deleted"), {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    render() {
        const { t } = this.props
        return (
            <div className='delete-form'>
                <button
                    onClick={() => {
                        if (this.props.saveSelected) {
                            this.setState({
                                showDelete: true,
                            })
                        }
                    }}>
                    {t("deleteHover.Delete server")}
                </button>
                {this.state.showDelete ? (
                    <>
                        <div className='delete-hover'>
                            <div className='delete-check'>
                                <p className='delete-text'>
                                    {t("deleteHover.Do you want to delete  ")}{this.props.saveSelected} {t("deleteHover.from server-monitoring?")}
                                </p>
                                <button
                                    onClick={() => {
                                        this.setState({
                                            showDelete: false,
                                        })
                                        this.deleteServer()
                                    }}>
                                    {t("deleteHover.Ok")}
                                </button>
                                <button
                                    onClick={() => {
                                        this.setState({
                                            showDelete: false,
                                        })
                                    }}>
                                    {t("deleteHover.Cancel")}
                                </button>
                            </div>
                        </div>
                    </>
                ) : null}
            </div>
        )
    }
}

export default withTranslation()(DeleteHover)
