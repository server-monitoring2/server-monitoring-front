import React from "react"
import { API_ENDPOINT } from "../../api"

import { withTranslation } from "react-i18next"
import "./AccountHover.sass"
class AccountHover extends React.Component {
    state = {
        showDelete: false,
    }

    deleteAccount = () => {
        const {
            i18n: { language },
        } = this.props
        fetch(`${API_ENDPOINT}/user/delete`, {
            method: "DELETE",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
        })
    }

    render() {
        const { t } = this.props
        return (
            <div className='account-form'>
                <button
                    onClick={() => {

                        this.setState({
                            showDelete: true,
                        })
                        

                        
                    }}>
                    {t("accountHover.Delete account")}
                </button>
                {this.state.showDelete ? (
                    <>
                        <style global jsx>
                            {`
                            .header-user{
                                z-index: 2
                            }
                            `}

                        </style>
                        <div className='account-hover'>
                            <div className='account-check'>
                                <p className='account-text'>
                                    {t("accountHover.Do you want to delete this account?")}
                                </p>
                                <button
                                    onClick={() => {
                                        this.setState({
                                            showDelete: false,
                                        })
                                        this.props.setToken(null)
                                        localStorage.removeItem('token')
                                        this.deleteAccount()
                                    }}>
                                    {t("accountHover.Ok")}
                                </button>
                                <button
                                    onClick={() => {
                                        this.setState({
                                            showDelete: false,
                                        })
                                    }}>
                                    {t("accountHover.Cancel")}
                                </button>
                            </div>
                        </div>
                    </>
                ) : null}
            </div>
        )
    }
}

export default withTranslation()(AccountHover)
