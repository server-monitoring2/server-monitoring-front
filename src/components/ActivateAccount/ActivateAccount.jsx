import React from "react"
import { withRouter } from "react-router-dom"
import { withTranslation } from "react-i18next"
import { API_ENDPOINT } from "../../api"
import { toast } from "react-toastify"
import "./ActivateAccount.sass"

class ActivateAccount extends React.Component {
    state = {
        ready: false,
    }

    componentDidMount() {
        const {
            i18n: { language },
            history,
        } = this.props
        fetch(
            `${API_ENDPOINT}/user/activate/${this.props.match.params.token}`,
            {
                headers: {
                    "Accept-Language": language,
                },
            }
        )
            .then(res => res.json())
            .then(res => {
                if (res.is_active === true) {
                    history.push("/login")
                    this.notifyS()
                } else if (res.error) {
                    history.push("/login")
                    this.notify(res.error)
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("activateAccount.Successfully registered"), {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    render() {
        const { t } = this.props
        document.title = t("activateAccount.title")
        return null
    }
}

export default withTranslation()(withRouter(ActivateAccount))
