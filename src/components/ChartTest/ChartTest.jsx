import React, { Component } from "react"
import { toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import "./ChartTest.sass"
export default class ChartTest extends Component {
    state = {}

    notify = () => {
        toast.info("Кулити", {
            position: "top-right",
            autoClose: 15000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.notify}>Notify !</button>
                
            </div>
        )
    }
}
