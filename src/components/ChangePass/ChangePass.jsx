import React from "react"
import { API_ENDPOINT } from "../../api"
import NotFound from "../NotFound/NotFound"
import InternalError from "../InternalError/InternalError"

import { withTranslation } from "react-i18next"

class ChangePass extends React.Component {
    state = {
        newPassword: "",
        oldPassword: "",
        isCommon: true,
        cAlert: "",
        vAlert: "",
        showAlert: true,
        showSuccess: true,
        errorStatus: 0,
        showChangePassword: false,
        isValidNewPassrord: true,
        isValidOldPasswordF: true,
        isValidOldPasswordS: true,
        errorNewPassword: "",
        errorOldPassword: "",
    }

    handleValidation = (oldPass, newPass) => {
        var a = false
        var b = false
        if (oldPass.toString() < 1) {
            this.setState({
                isValidOldPasswordF: false,
                errorOldPassword: "input-error",
            })
        } else if (oldPass.toString() === newPass) {
            this.setState({
                isValidOldPasswordS: false,
                errorOldPassword: "input-error",
            })
        } else {
            this.setState({
                isValidOldPasswordF: true,
                isValidOldPasswordS: true,
                errorOldPassword: "",
            })
            a = true
        }

        if (
            //eslint-disable-next-line
            /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@#^$!%*?&._]{8,}$/.test(
                newPass.toString()
            )
        ) {
            this.setState({
                isValidNewPassrord: true,
                errorNewPassword: "",
            })
            b = true
        } else {
            this.setState({
                isValidNewPassrord: false,
                errorNewPassword: "input-error",
            })
        }

        if (a && b) {
            this.changePass()
        }
    }

    changePass = () => {
        const {
            i18n: { language },
        } = this.props
        this.setState({
            showAlert: true,
            showSuccess: true,
        })
        fetch(`${API_ENDPOINT}/user/password/change`, {
            method: "POST",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
            body: JSON.stringify({
                old_password: this.state.oldPassword,
                new_password: this.state.newPassword,
            }),
        })
            .then(res => {
                if (res.status === 404) {
                    this.setState({ errorStatus: 404 })
                }
                if (res.status === 502) {
                    this.setState({ errorStatus: 502 })
                }
                return res.json()
            })
            .then(res => {
                if (res.error) {
                    this.setState({ showAlert: false })
                    this.setState({ cAlert: res.error })
                } else {
                    this.setState({ showSuccess: false })
                }
            })
    }
    render() {
        const { t } = this.props
        if (this.state.errorStatus === 404) {
            return <NotFound />
        }
        if (this.state.errorStatus === 502) {
            return <InternalError />
        }
        return (
            <div className='PassForm'>
                <div className="input-field'">
                    <button
                        onClick={() => {
                            this.setState(state => ({
                                showChangePassword: !state.showChangePassword,
                            }))
                        }}>
                        {t("changePass.Change Pass")}
                    </button>
                    {this.state.showChangePassword ? (
                        <>
                            <input
                                placeholder={t("changePass.Current password")}
                                type='password'
                                className={
                                    "validate " + this.state.errorOldPassword
                                }
                                value={this.state.oldPassword}
                                onChange={e =>
                                    this.setState({
                                        oldPassword: e.target.value,
                                    })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidOldPasswordF: false,
                                            errorOldPassword: "input-error",
                                        })
                                    } else if (
                                        e.target.value.toString() ===
                                        this.state.newPassword
                                    ) {
                                        this.setState({
                                            isValidOldPasswordS: false,
                                            errorOldPassword: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidOldPasswordF: true,
                                            isValidOldPasswordS: true,
                                            errorOldPassword: "",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorOldKey !== "") {
                                        this.setState({
                                            errorOldPassword: "",
                                            isValidOldPasswordF: true,
                                            isValidOldPasswordS: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.oldPassword,
                                            this.state.newPassword
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidOldPasswordF ? null : (
                                <p>
                                    {t("changePass.This field can't be empty")}
                                </p>
                            )}
                            {this.state.isValidOldPasswordS ? null : (
                                <p>{t("changePass.Same passwords")}</p>
                            )}
                            <input
                                placeholder={t("changePass.New password")}
                                type='password'
                                className={
                                    "validate " + this.state.errorNewPassword
                                }
                                value={this.state.newPassword}
                                onChange={e =>
                                    this.setState({
                                        newPassword: e.target.value,
                                    })
                                }
                                onBlur={e => {
                                    if (
                                        //eslint-disable-next-line
                                        /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@#^$!%*?&._]{8,}$/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        if (
                                            e.target.value.toString() ===
                                            this.state.oldPassword
                                        ) {
                                            this.setState({
                                                isValidOldPasswordS: false,

                                                errorOldPassword: "input-error",
                                            })
                                        } else {
                                            this.setState({
                                                isValidNewPassrord: true,
                                                errorNewPassword: "",
                                            })
                                        }
                                    } else {
                                        this.setState({
                                            isValidNewPassrord: false,
                                            errorNewPassword: "input-error",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorNewPassword !== "") {
                                        this.setState({
                                            errorNewPassword: "",
                                            isValidNewPassrord: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.oldPassword,
                                            this.state.newPassword
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidNewPassrord ? null : (
                                <p>{t("changePass.Password require")}</p>
                            )}
                            <button
                                disabled={
                                    this.state.oldPassword === "" ||
                                    this.state.newPassword === "" ||
                                    this.state.isValidOldPasswordF === false ||
                                    this.state.isValidOldPasswordS === false ||
                                    this.state.isValidNewPassrord === false
                                }
                                onClick={() => {
                                    this.changePass()
                                }}>
                                {t("changePass.Confirm")}
                            </button>

                            {this.state.showAlert ? null : (
                                <p>{this.state.cAlert}</p>
                            )}
                            {this.state.showSuccess ? null : (
                                <p>
                                    {t(
                                        "changePass.Password successful changed"
                                    )}
                                </p>
                            )}
                        </>
                    ) : null}
                </div>
            </div>
        )
    }
}

export default withTranslation()(ChangePass)
