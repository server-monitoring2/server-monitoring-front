import React from "react"
import { withTranslation } from "react-i18next"
import ReactCountryFlag from "react-country-flag"
import "./VisualConfig.sass"

class VisualConfig extends React.Component {
    state = {}

    changeLanguage = lng => {
        const { i18n } = this.props

        i18n.changeLanguage(lng)
    }
    render() {
        const {t} = this.props
        return (
            <div className='vis'>
                <p className='language'>{t("visualConfig.Language")}:</p>
                <ReactCountryFlag
                    countryCode='gb'
                    svg
                    style={{
                        width: "40px",
                        height: "40px",
                        margin: "10px",
                    }}
                    onClick={() => this.changeLanguage("en")}
                />
                {/* <ReactCountryFlag
                    countryCode='ru'
                    svg
                    style={{
                        width: "40px",
                        height: "40px",
                        margin: "10px",
                    }}
                    onClick={() => this.changeLanguage("ru")}
                />
                <ReactCountryFlag
                    countryCode='de'
                    svg
                    style={{
                        width: "40px",
                        height: "40px",
                        margin: "10px",
                    }}
                    onClick={() => this.changeLanguage("de")}
                /> */}
                <ReactCountryFlag
                    countryCode='ua'
                    svg
                    style={{
                        width: "40px",
                        height: "40px",
                        margin: "10px",
                    }}
                    onClick={() => this.changeLanguage("ua")}
                />
            </div>
        )
    }
}

export default withTranslation()(VisualConfig)
