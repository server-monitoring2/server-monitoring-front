import React from "react"
import { Link } from "react-router-dom"
import { withTranslation } from "react-i18next"
import "./AcceptSite.sass"

class AcceptSite extends React.Component {
    render() {
        const { t } = this.props
        if (this.props.showHover) {
            return (
                <div className='hover-site'>
                    <div className='header-site'>
                        <p>{t("acceptSite.text")}
                        <Link target='_blank' to='/documentation'>
                            {t("acceptSite.Term")}
                        </Link>
                        </p>
                        <button
                            onClick={() => {
                                localStorage.setItem("showPreloader", true)
                                this.props.setHover(false)
                            }}>
                            {t("acceptSite.Accept")}
                        </button>
                        <button
                            onClick={() => {
                                this.props.setToken(null)
                                localStorage.removeItem("token")
                            }}>
                            {t("acceptSite.Cancel")}
                        </button>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }
}

export default withTranslation()(AcceptSite)
