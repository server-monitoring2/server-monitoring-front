import React from "react"
import { toast } from "react-toastify"
import { withTranslation } from "react-i18next"
import "./AddUrl.sass"
import { API_ENDPOINT } from "../../api"
import urlRegex from "url-regex"

class AddUrl extends React.Component {
    state = {
        publicKey: "",
        privateKey: "",
        cAlert: "",
        showAlert: true,
        showSuccess: true,
        errorStatus: 0,
        showAddServer: false,
        isValidPublicKey: true,
        isValidPrivateKey: true,
        errorPublicKey: "",
        errorPrivateKey: "",
    }

    handleValidation = public_key => {
        var a = false
        if (public_key.length < 1) {
            this.setState({
                isValidPublicKey: false,
                errorPublicKey: "input-error",
            })
        } else if (
            //eslint-disable-next-line
            urlRegex({ exact: true }).test(public_key.toString())
        ) {
            if (
                //eslint-disable-next-line
                /[`{}";^'<>]/.test(public_key.toString())
            ) {
                this.setState({
                    isValidPublicKey: false,
                    errorPublicKey: "input-error",
                })
            } else {
                this.setState({
                    isValidPublicKey: true,
                    errorPublicKey: "",
                })
                a = true
            }
        } else {
            this.setState({
                isValidPublicKey: false,
                errorPublicKey: "input-error",
            })
        }

        if (a) {
            this.addServer()
        }
    }

    addServer = () => {
        const {
            i18n: { language },
        } = this.props
        let idx = this.state.publicKey.indexOf("#")
        console.log(idx)
        let urlSend = this.state.publicKey
        if (idx > 0) {
            urlSend = this.state.publicKey.substring(0, idx)
        }
        this.setState({ showAlert: true, showSuccess: true })
        fetch(`${API_ENDPOINT}/user/url/add`, {
            method: "POST",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
            body: JSON.stringify({
                url: urlSend,
            }),
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    this.notify(res.error)
                } else {
                    this.notifyS()

                    fetch(`${API_ENDPOINT}/sites/names`, {
                        headers: {
                            Authorization: this.props.token,
                            "Accept-Language": language,
                        },
                    })
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                if (res.error === "Invalid token") {
                                    this.props.setToken(null)
                                }
                            } else {
                                this.props.setServerName(res)
                            }
                        })
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("addUrl.Url added"), {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    render() {
        const { t } = this.props
        return (
            <div className='PassForm'>
                <div className='input-field'>
                    <button
                        onClick={() => {
                            this.setState(state => ({
                                showAddServer: !state.showAddServer,
                            }))
                        }}>
                        {t("addUrl.Add Url")}
                    </button>

                    {this.state.showAddServer ? (
                        <>
                            <input
                                placeholder={t("addUrl.Type your url here")}
                                type='text'
                                className={
                                    "validate " + this.state.errorPublicKey
                                }
                                value={this.state.publicKey}
                                onChange={e =>
                                    this.setState({ publicKey: e.target.value })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidPublicKey: false,
                                            errorPublicKey: "input-error",
                                        })
                                    } else if (
                                        //eslint-disable-next-line
                                        urlRegex({ exact: true }).test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        if (
                                            //eslint-disable-next-line
                                            /[`{}";^'<>]/.test(
                                                e.target.value.toString()
                                            )
                                        ) {
                                            this.setState({
                                                isValidPublicKey: false,
                                                errorPublicKey: "input-error",
                                            })
                                        } else {
                                            this.setState({
                                                isValidPublicKey: true,
                                                errorPublicKey: "",
                                            })
                                        }
                                    } else {
                                        this.setState({
                                            isValidPublicKey: false,
                                            errorPublicKey: "input-error",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorPublicKey !== "") {
                                        this.setState({
                                            errorPublicKey: "",
                                            isValidPublicKey: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.publicKey
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidPublicKey ? null : (
                                <p>{t("addUrl.Enter valid url")}</p>
                            )}
                            <button
                                disabled={
                                    this.state.publicKey === "" ||
                                    this.state.isValidPublicKey === false
                                }
                                onClick={() => {
                                    this.addServer()
                                }}>
                                {t("addUrl.Confirm")}
                            </button>
                        </>
                    ) : null}
                </div>
            </div>
        )
    }
}

export default withTranslation()(AddUrl)
