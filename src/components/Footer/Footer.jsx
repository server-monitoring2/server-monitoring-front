import React from "react"
import { Link} from "react-router-dom"

import { withTranslation } from "react-i18next"
import "./Footer.sass"

class Footer extends React.Component {
    render() {
        const { t } = this.props
        return (
             <footer>
                    <a
                        target='_blank'
                        rel='noopener noreferrer'
                        href='https://api.t2.tss2020.site/documentation/'>
                        {t("footer.Documentation")}
                    </a>
                    /
                    <Link
                        target='_blank'
                     to='/documentation'>{t("footer.Term of rules")}</Link>
                    /
                    <a href='mailto:t2@tss2020.repositoryhosting.com'>
                        {t("footer.Contact us")}
                    </a>
                    
                    
                </footer> 
        )
    }
}
export default withTranslation()(Footer)
