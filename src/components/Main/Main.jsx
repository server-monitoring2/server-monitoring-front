import React from "react"
import { Redirect } from "react-router-dom"
import VisualConfig from "../VisualConfig/VisualConfig"
import AgentMain from "../AgentTable/AgentMain"
import UrlMain from "../UrlMain/UrlMain"
import Footer from "../Footer/Footer"
import Header from "../Header/Header"
import AcceptSite from "../AcceptSite/AcceptSite"
import { toast } from "react-toastify"
import { withTranslation } from "react-i18next"
import "./Main.sass"

class Main extends React.Component {
    state = {
        rangeDays: null,
        showHover : true,


    }

    setHover = value =>{
        this.setState({
            showHover : value
        })

    }

    setRange = value => {
        this.setState({ rangeDays: value })
    }

    render() {
        toast.dismiss()
        const {t} = this.props 
        document.title = t("main.title")
        if (!this.props.token) {
            return <Redirect to='/login' exact />
        }
        return (
            <div className='header-main'>
                <div className='header-user'>
                    <Header
                        setRange = {this.setRange}
                        token={this.props.token}
                        setToken={this.props.setToken}
                    />
                </div>
                <div className='header-table'>
                    <AgentMain
                        rangeDays = {this.state.rangeDays}
                        token={this.props.token}
                        setToken={this.props.setToken}
                    />
                </div>
                <div className='header-url'>
                    <UrlMain
                        token={this.props.token}
                        setToken={this.props.setToken}
                    />
                </div>
                <VisualConfig />
                <Footer />
                {!localStorage.showPreloader &&   (
                    
                    <AcceptSite
                    showHover = {this.state.showHover}
                    setHover = {this.setHover}
                    setToken={this.props.setToken}
                    />
                )}
            </div>
        )
    }
}

export default withTranslation()(Main)
