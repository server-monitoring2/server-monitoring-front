import React from "react"
import { API_ENDPOINT } from "../../api"
import Select from "react-select"
import moment from 'moment';
import { withTranslation } from "react-i18next"
import "./AgentTable.sass"
import AddServer from "../AddServer/AddServer"
import ChangeServer from "../ChangeServer/ChangeServer"
import DeleteHover from "../ConfirmHover/DeleteHover"

class AgentTable extends React.Component {
    state = {
        agentData: [],
        serverNames: [],
        name: "",
        nameVisible: true,
        showAddServer: false,
        showChangeServer: false,
        showSuccess: true,
        selectedServerName: "",
        isLoaded: true,
        selectedValue: null,
        timer: null,
    }

    componentDidMount() {
        const {
            i18n: { language },
        } = this.props
        fetch(`${API_ENDPOINT}/server/names`, {
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    if (res.error === "Invalid token") {
                        this.props.setToken(null)
                    }
                } else {
                    this.setState({
                        serverNames: res.map(obj => ({
                            label: obj.public_key,
                            value: obj.public_key,
                        })),
                    })
                }
            })
    }

    componentWillUnmount() {
        clearInterval(this.state.timer)
    }

    setServerName = servName => {
        this.setState({
            serverNames: servName.map(obj => ({
                label: obj.public_key,
                value: obj.public_key,
            })),
        })
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.firstDate !== this.props.firstDate ||
            prevProps.secondDate !== this.props.secondDate
        ) {
            if(this.props.firstDate !== null && this.props.secondDate !== null){
                this.props.setRender(23)
            }
            else{
                this.props.setRender(null)
            }
            this.doRequest(
                this.state.name
            )
        }
    }

    doRequestStart = id => {
        if (this.props.firstDate === null && this.props.secondDate === null) {
            this.doRequest(id)
        } else {
            this.props.setDiapason(!this.props.changeDiapason)
        }
    }

    stopTimer = () =>{
        if (this.state.timer !== null) {
            clearInterval(this.state.timer)
        }
        this.props.setData([])
    }

    doRequest = id => {
        this.setState({
            isLoaded: false,
        })
        var firstD = new Date()
        var secondD = new Date()
        var today = new Date()
        var threeDays = new Date()
        threeDays.setDate(today.getDate() - 3)
        var sevenDays = new Date()
        sevenDays.setDate(today.getDate() - 7)
        var days = new Date()
        if (this.props.firstDate === null && this.props.secondDate === null) {
            if (this.props.rangeDays === 3) {
                days = threeDays
            } else if (this.props.rangeDays === 7) {
                days = sevenDays
            }
            firstD = days
            secondD = today


        } else {
            firstD = this.props.firstDate
            secondD = this.props.secondDate

        }

        let f= moment(firstD).utcOffset(0).format("yyyy-MM-DD HH:mm:ss Z")
        let s= moment(secondD).utcOffset(0).format("yyyy-MM-DD HH:mm:ss Z")

        const {
            i18n: { language },
        } = this.props
        fetch(`${API_ENDPOINT}/user/agent/list/${id}`, {
            method: "POST",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
            body: JSON.stringify({
                first_date: f,
                second_date: s,
            }),
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                    isLoaded: true,
                })
                if (res.error) {
                } else {
                    this.setState({
                        agentData: res,
                        nameVisible: false,
                    })
                    this.props.setData(res)
                }
            })
        if (this.state.timer !== null) {
            clearInterval(this.state.timer)
        }
        this.setState({
            timer: setInterval(() => {
                const {
                    i18n: { language },
                } = this.props
                fetch(`${API_ENDPOINT}/user/agent/list/${id}`, {
                    method: "POST",
                    headers: {
                        Authorization: this.props.token,
                        "Accept-Language": language,
                    },
                    body: JSON.stringify({
                        first_date: f,
                        second_date: s,
                    }),
                })
                    .then(res => res.json())
                    .then(res => {
                        this.setState({
                            isLoaded: true,
                        })
                        if (res.error) {
                        } else {
                            this.setState({
                                agentData: res,
                                nameVisible: false,
                            })
                            this.props.setData(res)
                        }
                    })
            }, 70000),
        })
    }

    render() {
        const { t } = this.props

        return (
            <div className='data-main'>
                <div className='data-button'>
                    <ul>
                        <li>
                            <AddServer
                                token={this.props.token}
                                setServerName={this.setServerName}
                            />
                        </li>
                        <li>
                            <ChangeServer
                                token={this.props.token}
                                setServerName={this.setServerName}
                            />
                        </li>
                        <li>
                            <DeleteHover
                                setSelectedValue={() => {
                                    this.setState({
                                        selectValue: null,
                                    })
                                }}
                                setSelectedServer={() => {
                                    this.setState({
                                        saveSelected: null,
                                    })
                                }}
                                token={this.props.token}
                                setServerName={this.setServerName}
                                saveSelected={this.state.saveSelected}
                                setVisible={() => {
                                    this.setState({
                                        nameVisible: true,
                                    })
                                }}
                                stopTimer = {this.stopTimer}
                            />
                        </li>
                    </ul>
                </div>
                <div className='data-info'>
                    <Select
                        options={this.state.serverNames}
                        className='select-component'
                        value={this.state.selectValue}
                        onChange={e => {
                            if (e.value !== this.state.saveSelected) {
                                this.setState({
                                    selectValue: e,
                                    name: e.value,
                                    saveSelected: e.value,
                                })
                                this.doRequestStart(e.value)
                            }
                        }}
                    />

                    {this.state.isLoaded ? null : (
                        <div className='loading'>
                            <div class='lds-ellipsis'>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    )}
                    {this.state.nameVisible ? null : (
                        <p>
                            {t("agentTable.Host")}: {this.state.name}
                        </p>
                    )}
                </div>
            </div>
        )
    }
}

export default withTranslation()(AgentTable)
