// import React from "react"
// import "./AgentTableItem.sass"

// class AgentTableItem extends React.Component {
//     render() {
//         const {
//             agent: { at, cpu, memory, disks },
//         } = this.props
//         return (
            
//             <div className='box'>
//                 <p className='agent'>Time: {at}</p>
//                 <p className='agent'>
//                     CPU:{" "}
//                     {cpu.map(cpuData => (
//                         <p className='agent'>
//                             Load on {(cpuData.user + cpuData.system).toFixed(2)}
//                             %, free {cpuData.idle.toFixed(2)}%
//                         </p>
//                     ))}
//                 </p>
//                 <p className='agent'>
//                     Memory:
//                     <p className='agent'>
//                         Active {(memory.active / 1024 / 1024).toFixed(2)}mb
//                     </p>
//                 </p>
//                 <p className='agent'>
//                     Disk:
//                     {disks.map(disk => (
//                         <p className='agent'>
//                             Load on{" "}
//                             {((disk.free / disk.total) * 100).toFixed(2)}% (
//                             {(disk.free / 1024 / 1024 / 1024).toFixed(2)}gb/
//                             {(disk.total / 1024 / 1024 / 1024).toFixed(2)}gb)
//                         </p>
//                     ))}
//                 </p>
//             </div>
//         )
//     }
// }

// export default AgentTableItem
