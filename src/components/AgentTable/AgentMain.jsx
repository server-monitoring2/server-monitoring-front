import React from "react"
import AgentTable from "../AgentTable/AgentTable"
import AgentTableChart from "../AgentTable/AgentTableChart"
import "./AgentMain.sass"

class AgentMain extends React.Component {
    state = {
        selectedServerName: [],
        firstDate: null,
        secondDate: null,
        changeDiapason: false,
        reRender: null,
    }
    setSeverName = value => {
        this.setState({ selectedServerName: value })
    }
    setRender = value => {
        this.setState({ reRender: value })
    }

    setChosenDate = (firstDate, secondDate) => {
        this.setState({
            firstDate: firstDate,
            secondDate: secondDate,
        })
    }

    setDiapason = value => {
        this.setState({
            changeDiapason: value,
        })
    }

    render() {
        return (
            <div className='agent-header-main'>
                <div className='agent-header-select'>
                    <AgentTable
                        setRender={this.setRender}
                        rangeDays={this.props.rangeDays}
                        changeDiapason={this.state.changeDiapason}
                        setDiapason={this.setDiapason}
                        setData={this.setSeverName}
                        token={this.props.token}
                        setToken={this.props.setToken}
                        firstDate={this.state.firstDate}
                        secondDate={this.state.secondDate}
                    />
                </div>
                <div className='agent-header-table'>
                    <AgentTableChart
                        reRender={this.state.reRender}
                        changeDiapason={this.state.changeDiapason}
                        rangeDays={this.props.rangeDays}
                        agent={this.state.selectedServerName}
                        setChosenDate={this.setChosenDate}
                    />
                </div>
            </div>
        )
    }
}

export default AgentMain
