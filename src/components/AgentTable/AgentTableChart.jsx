import React from "react"

import { withTranslation } from "react-i18next"
import { Line } from "react-chartjs-2"
import DateTimePicker from "react-datetime-picker"
import "./AgentTableChart.sass"
class AgentTable extends React.Component {
    state = {
        graphData: [],
        avarageCPU: null,
        avarageMemory: null,
        avarageDisk: null,
        choosenF: null,
        choosenS: null,
        pickedDateF: null,
        pickedDateS: null,
        validateDiapason: true,
    }

    setGradientColor = (canvas, color) => {
        const ctx = canvas.getContext("2d")
        const gradient = ctx.createLinearGradient(0, 0, 600, 550)
        gradient.addColorStop(0, color)
        gradient.addColorStop(0.95, "rgba(133,122, 144, 0.5)")
        return gradient
    }

    dateChangeF = value => {
        this.setState({
            choosenF: value,
            pickedDateF: value,
        })
    }
    dateChangeS = value => {
        this.setState({
            choosenS: value,
            pickedDateS: value,
        })
    }

    handleDiapason = (pickedDateF, pickedDateS) => {
        if (pickedDateF >= pickedDateS) {
            this.setState({
                validateDiapason: false,
            })
            setTimeout(() => {
                this.setState({
                    validateDiapason: true,
                })
            }, 2000)
        } else {

            this.props.setChosenDate(pickedDateF.toString(), pickedDateS.toString())
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.agent !== this.props.agent) {
            this.getChartData()
        }
        if (prevProps.changeDiapason !== this.props.changeDiapason) {
            this.setState({
                choosenF: null,
                choosenS: null,
                pickedDateF: null,
                pickedDateS: null,
            })
            this.props.setChosenDate(null, null)
        }
    }

    getChartData = () => {
        let cpuArr = null
        let memoryArr = null
        let diskArr = null

        this.props.agent.forEach(obj => {
            cpuArr = cpuArr + (obj.cpu[0].user + obj.cpu[0].system)
            memoryArr = memoryArr + obj.memory.active / 1024 / 1024
            diskArr = diskArr + (obj.disks[0].free / obj.disks[0].total) * 100
        })

        this.setState({
            avarageCPU: (cpuArr / this.props.agent.length).toFixed(2),
            avarageMemory: (memoryArr / this.props.agent.length).toFixed(2),
            avarageDisk: (diskArr / this.props.agent.length).toFixed(2),
        })

        this.setState({
            graphData: [
                {
                    labels:
                        this.props.agent &&
                        this.props.agent.map(({ at }) => at),

                    datasets: [
                        {
                            backgroundColor: "rgba(100,0,255, 0.5)",
                            borderColor: "white",
                            borderWidth: 2,
                            fill: "+2",
                            hoverBackgroundColor: "rgba(255,0,0, 0.5)",
                            hoverBorderColor: "rgba(255,0,0, 0.5)",
                            pointStyle: "circle",
                            pointRadius: 3,
                            fontColor: "red",
                            hoverBorderWidth: 10,
                            label: "CPU",
                            data:
                                this.props.agent &&
                                this.props.agent.map(
                                    ({ cpu }) => cpu[0].user + cpu[0].system
                                ),
                        },
                    ],
                },
                {
                    labels:
                        this.props.agent &&
                        this.props.agent.map(({ at }) => at),

                    datasets: [
                        {
                            backgroundColor: "rgba(0,255,100, 0.5)",
                            borderColor: "white",
                            borderWidth: 2,
                            fill: "+2",
                            hoverBackgroundColor: "rgba(255,0,0, 0.5)",
                            hoverBorderColor: "rgba(255,0,0, 0.5)",
                            pointStyle: "circle",
                            pointRadius: 3,
                            fontColor: "red",
                            hoverBorderWidth: 10,
                            label: "Memory",
                            data:
                                this.props.agent &&
                                this.props.agent.map(({ memory }) =>
                                    (memory.active / 1024 / 1024).toFixed(2)
                                ),
                        },
                    ],
                },
                {
                    labels:
                        this.props.agent &&
                        this.props.agent.map(({ at }) => at),

                    datasets: [
                        {
                            backgroundColor: "rgba(255,100,0, 0.5)",
                            borderColor: "white",
                            borderWidth: 2,
                            fill: "+2",
                            hoverBackgroundColor: "rgba(255,0,0, 0.5)",
                            hoverBorderColor: "rgba(255,0,0, 0.5)",
                            pointStyle: "circle",
                            pointRadius: 3,
                            fontColor: "red",
                            hoverBorderWidth: 10,
                            label: "Disk",
                            data:
                                this.props.agent &&
                                this.props.agent.map(({ disks }) =>
                                    (
                                        (disks[0].free / disks[0].total) *
                                        100
                                    ).toFixed(2)
                                ),
                        },
                    ],
                },
            ],
        })
    }
    render() {
        const {
            t,
            i18n: { language },
        } = this.props
        var pickerLanguage = language

        if (pickerLanguage === "ua") {
            pickerLanguage = "ru"
        }

        var today = new Date()
        var threeDays = new Date()
        threeDays.setDate(today.getDate() - 3)
        var sevenDays = new Date()
        sevenDays.setDate(today.getDate() - 7)
        var days = new Date()
        if (this.props.rangeDays === 3) {
            days = threeDays
        } else if (this.props.rangeDays === 7) {
            days = sevenDays
        }
        if (this.props.agent.length === 0) {
            if (this.props.reRender === null) {
                return <p>{t("agentTableChart.Nothing to show yet")}</p>
            } else {
                return (
                    <div className='header-chart'>
                        <p>{t("agentTableChart.Nothing to show yet")}</p>
                        <p>{t("agentTableChart.range")}</p>
                        <div className='diapason'>
                            <p>{t("agentTableChart.From")}</p>
                            <DateTimePicker
                                format='y-MM-dd H'
                                className='picker'
                                locale={pickerLanguage}
                                minDate={days}
                                maxDetail='hour'
                                maxDate={today}
                                value={this.state.choosenF}
                                onChange={value => this.dateChangeF(value)}
                            />
                            <p>{t("agentTableChart.To")}</p>
                            <DateTimePicker
                                format='y-MM-dd H'
                                className='picker'
                                locale={pickerLanguage}
                                minDate={days}
                                maxDetail='hour'
                                maxDate={today}
                                value={this.state.choosenS}
                                onChange={value => this.dateChangeS(value)}
                            />
                            <button
                                className='diapason-button'
                                disabled={
                                    this.state.pickedDateF === null ||
                                    this.state.pickedDateS === null ||
                                    Number.isNaN(this.state.pickedDateF) ===
                                        true ||
                                    Number.isNaN(this.state.pickedDateS) ===
                                        true
                                }
                                onClick={() =>
                                    this.handleDiapason(
                                        this.state.pickedDateF,
                                        this.state.pickedDateS
                                    )
                                }>
                                {t("agentTableChart.Confirm")}
                            </button>
                        </div>
                        {this.state.validateDiapason ? null : (
                            <p className='wrong-diapason'>
                                {t("agentTableChart.Wrong diapason")}
                            </p>
                        )}
                    </div>
                )
            }
        } else
            return (
                <div className='header-chart'>
                    <p>{t("agentTableChart.range")}</p>
                    <div className='diapason'>
                        <p>{t("agentTableChart.From")}</p>
                        <DateTimePicker
                            format='y-MM-dd H'
                            className='picker'
                            locale={pickerLanguage}
                            minDate={days}
                            maxDetail='hour'
                            maxDate={today}
                            value={this.state.choosenF}
                            onChange={value => this.dateChangeF(value)}
                        />
                        <p>{t("agentTableChart.To")}</p>
                        <DateTimePicker
                            format='y-MM-dd H'
                            className='picker'
                            locale={pickerLanguage}
                            minDate={days}
                            maxDetail='hour'
                            maxDate={today}
                            value={this.state.choosenS}
                            onChange={value => this.dateChangeS(value)}
                        />
                        <button
                            className='diapason-button'
                            disabled={
                                this.state.pickedDateF === null ||
                                this.state.pickedDateS === null ||
                                Number.isNaN(this.state.pickedDateF) === true ||
                                Number.isNaN(this.state.pickedDateS) === true
                            }
                            onClick={() =>
                                this.handleDiapason(
                                    this.state.pickedDateF,
                                    this.state.pickedDateS
                                )
                            }>
                            {t("agentTableChart.Confirm")}
                        </button>
                    </div>
                    {this.state.validateDiapason ? null : (
                        <p className='wrong-diapason'>
                            {t("agentTableChart.Wrong diapason")}
                        </p>
                    )}
                    <div className='chart'>
                        <h3>{t("agentTableChart.CPU")}</h3>
                        <Line
                            className='graph'
                            options={{
                                maintainAspectRatio: false,
                                responsive: true,
                                scales: {
                                    xAxes: [
                                        {
                                            type: "time",
                                            time: {
                                                unit: "minute",
                                                round: "minute",
                                                tooltipFormat: "h:mm:ss a",
                                                displayFormats: {
                                                    hour: "MMM D, h:mm A",
                                                },
                                            },
                                        },
                                    ],
                                    yAxes: [
                                        {
                                            scaleLabel: {
                                                display: true,
                                                labelString: t(
                                                    "agentTableChart.Load in %"
                                                ),
                                                fontColor: "white",
                                                fontSize: 20,
                                            },
                                        },
                                    ],
                                },
                            }}
                            data={this.state.graphData[0]}
                        />
                    </div>
                    <p>
                        {t("agentTableChart.Avarage CPU load")} :{" "}
                        {this.state.avarageCPU}%
                    </p>
                    <div className='chart'>
                        <h3>{t("agentTableChart.Memory")}</h3>
                        <Line
                            className='graph'
                            options={{
                                maintainAspectRatio: false,
                                responsive: true,
                                scales: {
                                    xAxes: [
                                        {
                                            type: "time",
                                            time: {
                                                unit: "minute",
                                                round: "minute",
                                                tooltipFormat: "h:mm:ss a",
                                                displayFormats: {
                                                    hour: "MMM D, h:mm A",
                                                },
                                            },
                                        },
                                    ],
                                    yAxes: [
                                        {
                                            scaleLabel: {
                                                display: true,
                                                labelString: t(
                                                    "agentTableChart.Load in mb"
                                                ),
                                                fontColor: "white",
                                                fontSize: 20,
                                            },
                                        },
                                    ],
                                },
                            }}
                            data={this.state.graphData[1]}
                        />
                    </div>
                    <p>
                        {t("agentTableChart.Avarage memory load")} :{" "}
                        {this.state.avarageMemory}
                        {t("agentTableChart.mb")}
                    </p>
                    <div className='chart'>
                        <h3>{t("agentTableChart.Disk")}</h3>
                        <Line
                            className='graph'
                            options={{
                                maintainAspectRatio: false,
                                responsive: true,
                                scales: {
                                    xAxes: [
                                        {
                                            type: "time",
                                            time: {
                                                unit: "minute",
                                                round: "minute",
                                                tooltipFormat: "h:mm:ss a",
                                                displayFormats: {
                                                    hour: "MMM D, h:mm A",
                                                },
                                            },
                                        },
                                    ],
                                    yAxes: [
                                        {
                                            scaleLabel: {
                                                display: true,
                                                labelString: t(
                                                    "agentTableChart.Load in %"
                                                ),
                                                fontColor: "white",
                                                fontSize: 20,
                                            },
                                        },
                                    ],
                                },
                            }}
                            data={this.state.graphData[2]}
                        />
                    </div>
                    <p>
                        {t("agentTableChart.Avarage disk load")}:{" "}
                        {this.state.avarageDisk}%
                    </p>
                </div>
            )
    }
}

export default withTranslation()(AgentTable)
