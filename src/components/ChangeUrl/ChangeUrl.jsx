// import React from "react"

// import { API_ENDPOINT } from "../../api"
// import NotFound from "../NotFound/NotFound"
// import InternalError from "../InternalError/InternalError"

// class ChangeUrl extends React.Component {
//     state = {
//         oldPublicKey: "",
//         newPublicKey: "",
//         newPrivateKey: "",
//         cAlert: "",
//         showAlert: true,
//         showSuccess: true,
//         errorStatus: 0,
//         isValidOldKey: true,
//         isValidPublicKey: true,
//         isValidPrivateKey: true,
//         errorOldKey: "",
//         errorPublicKey: "",
//         errorPrivateKey: "",
//     }

//     handleValidation = (old_key, public_key) => {
//         var a = false
//         var b = false
//         if (old_key.length < 1) {
//             this.setState({
//                 errorMessage: "This field can't be empty",
//                 isValidOldKey: false,
//                 errorOldKey: "input-error",
//             })
//         } else if (old_key.toString() === public_key) {
//             this.setState({
//                 isValidOldKey: false,
//                 errorOldKey: "input-error",
//                 errorMessage: "Same url",
//             })
//         } else {
//             this.setState({
//                 isValidOldKey: true,
//                 errorOldKey: "",
//             })
//             a = true
//         }

//         if (public_key.length < 1) {
//             this.setState({
//                 isValidPublicKey: false,
//                 errorPublicKey: "input-error",
//             })
//         } else if (public_key.toString() === old_key) {
//             this.setState({
//                 isValidOldKey: false,
//                 errorOldKey: "input-error",
//                 errorMessage: "Same url",
//             })
//         } else if (
//             //eslint-disable-next-line
//             /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)/.test(
//                 public_key.toString()
//             )
//         ) {
//             this.setState({
//                 isValidPublicKey: true,
//                 errorPublicKey: "",
//             })
//             b = true
//         } else {
//             this.setState({
//                 isValidPublicKey: false,
//                 errorPublicKey: "input-error",
//             })
//         }

//         if (a && b) {
//             this.changeServer()
//         }
//     }

//     changeServer = () => {
//         this.setState({ showAlert: true, showSuccess: true })
//         fetch(`${API_ENDPOINT}/user/url/change`, {
//             method: "PUT",
//             headers: {
//                 Authorization: this.props.token,
//             },
//             body: JSON.stringify({
//                 old_url: this.state.oldPublicKey,
//                 new_url: this.state.newPublicKey,
//             }),
//         })
//             .then(res => res.json())
//             .then(res => {
//                 if (res.error) {
//                     this.setState({ showAlert: false })
//                     this.setState({ cAlert: res.error })
//                 } else {
//                     this.setState({ showSuccess: false })

//                     fetch(`${API_ENDPOINT}/sites/names`, {
//                         headers: {
//                             Authorization: this.props.token,
//                         },
//                     })
//                         .then(res => res.json())
//                         .then(res => {
//                             if (res.error) {
//                                 if (res.error === "Invalid token") {
//                                     this.props.setToken(null)
//                                 }
//                             } else {
//                                 this.props.setServerName(res)
//                             }
//                         })
//                 }
//             })
//     }
//     render() {
//         if (this.state.errorStatus === 404) {
//             return <NotFound />
//         }
//         if (this.state.errorStatus === 502) {
//             return <InternalError />
//         }
//         return (
//             <div className='PassForm'>
//                 <div className='input-field'>
//                     <button
//                         onClick={() => {
//                             this.setState(state => ({
//                                 showChangeServer: !state.showChangeServer,
//                             }))
//                         }}>
//                         Change Url
//                     </button>

//                     {this.state.showChangeServer ? (
//                         <>
//                             <input
//                                 placeholder='Current url'
//                                 type='text'
//                                 className={"validate " + this.state.errorOldKey}
//                                 value={this.state.oldPublicKey}
//                                 onChange={e =>
//                                     this.setState({
//                                         oldPublicKey: e.target.value,
//                                     })
//                                 }
//                                 onBlur={e => {
//                                     if (e.target.value.length < 1) {
//                                         this.setState({
//                                             errorMessage:
//                                                 "This field can't be empty",
//                                             isValidOldKey: false,
//                                             errorOldKey: "input-error",
//                                         })
//                                     } else if (
//                                         e.target.value.toString() ===
//                                         this.state.newPublicKey
//                                     ) {
//                                         this.setState({
//                                             isValidOldKey: false,
//                                             errorOldKey: "input-error",

//                                             errorMessage: "Same url",
//                                         })
//                                     } else {
//                                         this.setState({
//                                             isValidOldKey: true,
//                                             errorOldKey: "",
//                                         })
//                                     }
//                                 }}
//                                 onFocus={() => {
//                                     if (this.state.errorOldKey !== "") {
//                                         this.setState({
//                                             errorOldKey: "",
//                                             isValidOldKey: true,
//                                         })
//                                     }
//                                 }}
//                                 onKeyPress={event => {
//                                     if (event.key === "Enter") {
//                                         this.handleValidation(
//                                             this.state.oldPublicKey,
//                                             this.state.newPublicKey
//                                         )
//                                     }
//                                 }}
//                             />
//                             {this.state.isValidOldKey ? null : (
//                                 <p>{this.state.errorMessage}</p>
//                             )}
//                             <input
//                                 placeholder='New url'
//                                 type='text'
//                                 className={
//                                     "validate " + this.state.errorPublicKey
//                                 }
//                                 value={this.state.newPublicKey}
//                                 onChange={e =>
//                                     this.setState({
//                                         newPublicKey: e.target.value,
//                                     })
//                                 }
//                                 onBlur={e => {
//                                     if (e.target.value.length < 1) {
//                                         this.setState({
//                                             isValidPublicKey: false,
//                                             errorPublicKey: "input-error",
//                                         })
//                                     } else if (
//                                         e.target.value.toString() ===
//                                         this.state.oldPublicKey
//                                     ) {
//                                         this.setState({
//                                             isValidOldKey: false,
//                                             errorOldKey: "input-error",
//                                             errorMessage: "Same url",
//                                         })
//                                     } else if (
//                                         //eslint-disable-next-line
//                                         /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)/.test(
//                                             e.target.value.toString()
//                                         )
//                                     ) {
//                                         this.setState({
//                                             isValidPublicKey: true,
//                                             errorPublicKey: "",
//                                         })
//                                     } else {
//                                         this.setState({
//                                             isValidPublicKey: false,
//                                             errorPublicKey: "input-error",
//                                         })
//                                     }
//                                 }}
//                                 onFocus={() => {
//                                     if (this.state.errorPublicKey !== "") {
//                                         this.setState({
//                                             errorPublicKey: "",
//                                             isValidPublicKey: true,
//                                         })
//                                     }
//                                 }}
//                                 onKeyPress={event => {
//                                     if (event.key === "Enter") {
//                                         this.handleValidation(
//                                             this.state.oldPublicKey,
//                                             this.state.newPublicKey
//                                         )
//                                     }
//                                 }}
//                             />
//                             {this.state.isValidPublicKey ? null : (
//                                 <p>Enter valid url</p>
//                             )}
//                             <button
//                                 disabled={
//                                     this.state.oldPublicKey === "" ||
//                                     this.state.newPublicKey === "" ||
//                                     this.state.isValidOldKey === false ||
//                                     this.state.isValidPublicKey === false
//                                 }
//                                 onClick={() => {
//                                     this.changeServer()
//                                 }}>
//                                 Confirm
//                             </button>
//                             {this.state.showAlert ? null : (
//                                 <p>{this.state.cAlert}</p>
//                             )}
//                             {this.state.showSuccess ? null : <p>Url changed</p>}
//                         </>
//                     ) : null}
//                 </div>
//             </div>
//         )
//     }
// }

// export default ChangeUrl
