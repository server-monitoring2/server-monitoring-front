import React, { Component } from "react"
import { withRouter, Link } from "react-router-dom"
import { loadStripe } from "@stripe/stripe-js"
import {
    CardNumberElement,
    CardCvcElement,
    CardExpiryElement,
    Elements,
    ElementsConsumer,
} from "@stripe/react-stripe-js"
import { API_ENDPOINT } from "../../api"
import { withTranslation } from "react-i18next"
import Footer from "../Footer/Footer"

import "./StripeTest.sass"

class CreateEmailSupport extends Component {
    state = {
        errorPayment: "",
        errorShow: true,
    }



    handleSubmit = async () => {
        this.setState({
            errorShow: true,
        })
        const { stripe, elements, push, i18n:{language}} = this.props

        if (!stripe || !elements) {
            return
        }
        const res = await fetch(`${API_ENDPOINT}/user/payment`, {
            method: "POST",
            headers: {
                Authorization: localStorage.token,
                "Accept-Language" : language
            },
        })

        const json = await res.json()
        if (json.error) {
            this.setState({
                errorPayment: json.error,
                errorShow: false,
            })
        } else {
            const clientSecret = json.clientSecret
            const paymentMetod = await stripe.createPaymentMethod({
                type: "card",
                card: elements.getElement(CardNumberElement),
            })

            if (paymentMetod.error) {
                this.setState({
                    errorPayment: paymentMetod.error.message,
                    errorShow: false,
                })
            } else {
                const payLoad = await stripe.confirmCardPayment(
                    clientSecret,

                    {
                        payment_method: paymentMetod.paymentMethod.id,
                    }
                )
                if (payLoad.error) {
                    this.setState({
                        errorPayment: payLoad.error.message,
                        errorShow: false,
                    })
                } else {
                    setTimeout(() => {
                        push("/")
                    }, 2000)
                }
            }
        }
    }

    render() {
        const { t } = this.props
        document.title = t("stripe.title")
        const CARD_OPTIONS = {
             style: {
                 base: {
                     color: "#000000",
                     fontSize: "30px",
                     fontSmoothing: "antialiased",
                     "::placeholder": {
                         color: "rgba(120, 116, 120)",
                     },
                 },
                 invalid: {
                     color: "red",
                 },
             },
         }
         const CARD_OPTIONS1 = {
            style: {
                base: {
                    color: "#000000",
                    fontSize: "30px",
                    fontSmoothing: "antialiased",
                    "::placeholder": {
                        color: "rgba(120, 116, 120)",
                    },
                },
                invalid: {
                    color: "red",
                },
            },
            placeholder : "4242 4242 4242 4242"
        }
        return (
            <div className='create_email_support_page'>
                <p className='prior'>{t("stripe.Money is non-refundable")}</p>
                <style global jsx>
                    {`
                        body {
                            min-height: 100%;
                        }
                    `}
                </style>
                <div className='wrapper_content'>
                    <div className='card'>
                        <span>{t("stripe.Card number")}</span>
                        <div className='FormNotNumber'>
                            <CardNumberElement options={CARD_OPTIONS1} />
                        </div>
                    </div>
                    <div className='expiry'>
                        <span>{t("stripe.Expiry")}</span>
                        <div className='FormNotNumber'>
                            <CardExpiryElement options={CARD_OPTIONS} />
                        </div>
                    </div>
                    <div className='cvc'>
                        <span>CVC</span>
                        <div className='FormNotNumber'>
                            <CardCvcElement options={CARD_OPTIONS} />
                        </div>
                    </div>
                    <div className='form-btns'>
                        <button
                            className='btn_blue'
                            onClick={this.handleSubmit}>
                            {t("stripe.Pay")}
                        </button>
                    </div>
                    <Link to='/' exact>
                        {t("stripe.Back to cabinet")}
                    </Link>
                </div>
                {this.state.errorShow ? null : <p>{this.state.errorPayment}</p>}
            </div>
        )
    }
}

const EmailSupportForm = ({ history, t, i18n }) => {
    var stripeLanguage = i18n.language
    if(stripeLanguage === "ua"){
        stripeLanguage = "ru"
    }
    const stripePromise = loadStripe(
        "pk_test_51Hc6FDF1TdcqCa4T9wZE2gne6txldm7u3BD4LClne3HLAQTvZCirQnJviFoXcdByAd3sgDnH1oZsN5dpYJaJnarS00ZjqStU79",
        { locale: stripeLanguage}
    )

    return (
        <div>
            <Elements stripe={stripePromise}>
                <ElementsConsumer>
                    {({ stripe, elements }) => (
                        <CreateEmailSupport
                            stripe={stripe}
                            elements={elements}
                            push={history.push}
                            t={t}
                            i18n = {i18n}
                        />
                    )}
                </ElementsConsumer>
            </Elements>

            <Footer />
        </div>
    )
}

export default withTranslation()(withRouter(EmailSupportForm))
