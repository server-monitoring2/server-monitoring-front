import React from "react"
import { toast } from "react-toastify"
import { withTranslation } from "react-i18next"
import { API_ENDPOINT } from "../../api"
import NotFound from "../NotFound/NotFound"
import InternalError from "../InternalError/InternalError"

class ChangeServer extends React.Component {
    state = {
        oldPublicKey: "",
        newPublicKey: "",
        newPrivateKey: "",
        cAlert: "",
        showAlert: true,
        showSuccess: true,
        errorStatus: 0,
        isValidOldKeyF: true,
        isValidOldKeyS: true,
        isValidPublicKey: true,
        isValidPrivateKey: true,
        errorOldKey: "",
        errorPublicKey: "",
        errorPrivateKey: "",
    }

    handleValidation = (old_key, public_key, private_key) => {
        var a = false
        var b = false
        var c = false
        if (old_key.toString().length < 1) {
            this.setState({
                isValidOldKeyF: false,
                errorOldKey: "input-error",
            })
        } else if (old_key.toString() === public_key) {
            this.setState({
                isValidOldKeyS: false,
                errorOldKey: "input-error",

            })
        } else {
            this.setState({
                isValidOldKeyF: true,
                isValidOldKeyS: true,
                errorOldKey: "",
            })
            a = true
        }

        if (
            //eslint-disable-next-line
            /[#@!$^&~=`{}\[\]";'/<>]/.test(public_key.toString()) ||
            public_key.toString() < 1
        ) {
            this.setState({
                isValidPublicKey: false,
                errorPublicKey: "input-error",
            })
        } else {
            this.setState({
                isValidPublicKey: true,
                errorPublicKey: "",
            })
            b = true
        }

        if (
            //eslint-disable-next-line
            /[#@!$^&~=`{}\[\]";'/<>]/.test(private_key.toString()) ||
            private_key.toString() < 1
        ) {
            this.setState({
                isValidPrivateKey: false,
                errorPrivateKey: "input-error",
            })
        } else {
            this.setState({
                isValidPrivateKey: true,
                errorPrivateKey: "",
            })
            c = true
        }

        if (a && b && c) {
            this.changeServer()
        }
    }

    changeServer = () => {
        const {
            i18n: { language },
        } = this.props
        this.setState({ showAlert: true, showSuccess: true })
        fetch(`${API_ENDPOINT}/user/server/change`, {
            method: "PUT",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
            body: JSON.stringify({
                old_public_key: this.state.oldPublicKey,
                new_public_key: this.state.newPublicKey,
                new_private_key: this.state.newPrivateKey,
            }),
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    this.notify(res.error)
                } else {
                    this.notifyS()

                    fetch(`${API_ENDPOINT}/server/names`, {
                        headers: {
                            Authorization: this.props.token,
                            "Accept-Language": language,
                        },
                    })
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                if (res.error === "Invalid token") {
                                    this.props.setToken(null)
                                }
                            } else {
                                this.props.setServerName(res)
                            }
                        })
                }
            })
    }


    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("cahngeServer.Server changed"), {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }


    render() {
        const { t } = this.props
        if (this.state.errorStatus === 404) {
            return <NotFound />
        }
        if (this.state.errorStatus === 502) {
            return <InternalError />
        }
        return (
            <div className='PassForm'>
                <div className='input-field'>
                    <button
                        onClick={() => {
                            this.setState(state => ({
                                showChangeServer: !state.showChangeServer,
                            }))
                        }}>
                        {t("cahngeServer.Change Server")}
                    </button>

                    {this.state.showChangeServer ? (
                        <>
                            <input
                                placeholder={t(
                                    "cahngeServer.Current public key"
                                )}
                                type='text'
                                className={"validate " + this.state.errorOldKey}
                                value={this.state.oldPublicKey}
                                onChange={e =>
                                    this.setState({
                                        oldPublicKey: e.target.value,
                                    })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({

                                            isValidOldKeyF: false,
                                            errorOldKey: "input-error",
                                        })
                                    } else if (
                                        e.target.value.toString() ===
                                        this.state.newPublicKey
                                    ) {
                                        this.setState({
                                            isValidOldKeyS: false,
                                            errorOldKey: "input-error",

                                        })
                                    } else {
                                        this.setState({
                                            isValidOldKeyF: true,
                                            isValidOldKeyS: true,
                                            errorOldKey: "",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorOldKey !== "") {
                                        this.setState({
                                            errorOldKey: "",
                                            isValidOldKeyF: true,
                                            isValidOldKeyS: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.oldPublicKey,
                                            this.state.newPublicKey,
                                            this.state.newPrivateKey
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidOldKeyF ? null : (
                                <p>{t(
                                    "cahngeServer.This field can't be empty"
                                )}</p>
                            )}
                            {this.state.isValidOldKeyS ? null : (
                                <p>{t(
                                    "cahngeServer.Same public keys"
                                )}</p>
                            )}
                            <input
                                placeholder={t("cahngeServer.New public key")}
                                type='text'
                                className={
                                    "validate " + this.state.errorPublicKey
                                }
                                value={this.state.newPublicKey}
                                onChange={e =>
                                    this.setState({
                                        newPublicKey: e.target.value,
                                    })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidPublicKey: false,
                                            errorPublicKey: "input-error",
                                        })
                                    } else if (
                                        //eslint-disable-next-line
                                        /[#@!$^&~=`{}\[\]";'/<>]/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidPublicKey: false,
                                            errorPublicKey: "input-error",
                                        })
                                    } else if (
                                        e.target.value.toString() ===
                                        this.state.oldPublicKey
                                    ) {
                                        this.setState({
                                            isValidOldKeyS: false,

                                            errorOldKey: "input-error",

                                        })
                                    } else {
                                        this.setState({
                                            isValidPublicKey: true,
                                            errorPublicKey: "",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorPublicKey !== "") {
                                        this.setState({
                                            errorPublicKey: "",
                                            isValidPublicKey: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.oldPublicKey,
                                            this.state.newPublicKey,
                                            this.state.newPrivateKey
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidPublicKey ? null : (
                                <p>{t("cahngeServer.Validate")}</p>
                            )}
                            <input
                                placeholder={t("cahngeServer.New private key")}
                                type='text'
                                className={
                                    "validate " + this.state.errorPrivateKey
                                }
                                value={this.state.newPrivateKey}
                                onChange={e =>
                                    this.setState({
                                        newPrivateKey: e.target.value,
                                    })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidPrivateKey: false,
                                            errorPrivateKey: "input-error",
                                        })
                                    } else if (
                                        //eslint-disable-next-line
                                        /[#@!$^&~=`{}\[\]";'/<>]/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidPrivateKey: false,
                                            errorPrivateKey: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidPrivateKey: true,
                                            errorPrivateKey: "",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorPrivateKey !== "") {
                                        this.setState({
                                            errorPrivateKey: "",
                                            isValidPrivateKey: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.oldPublicKey,
                                            this.state.newPublicKey,
                                            this.state.newPrivateKey
                                        )
                                    }
                                }}
                            />

                            {this.state.isValidPrivateKey ? null : (
                                <p>{t("cahngeServer.Validate")}</p>
                            )}

                            <button
                                disabled={
                                    this.state.oldPublicKey === "" ||
                                    this.state.newPublicKey === "" ||
                                    this.state.newPrivateKey === "" ||
                                    this.state.isValidOldKeyF === false ||
                                    this.state.isValidOldKeyS === false ||
                                    this.state.isValidPublicKey === false ||
                                    this.state.isValidPrivateKey === false
                                }
                                onClick={() => {
                                    this.changeServer()
                                }}>
                                {t("cahngeServer.Confirm")}
                            </button>
                        </>
                    ) : null}
                </div>
            </div>
        )
    }
}

export default withTranslation()(ChangeServer)
