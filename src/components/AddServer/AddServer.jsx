import React from "react"
import { toast } from "react-toastify"
import { withTranslation } from "react-i18next"
import "./AddServer.sass"
import { API_ENDPOINT } from "../../api"

class AddServer extends React.Component {
    state = {
        publicKey: "",
        privateKey: "",
        cAlert: "",
        showAlert: true,
        showSuccess: true,
        errorStatus: 0,
        showAddServer: false,
        isValidPublicKey: true,
        isValidPrivateKey: true,
        errorPublicKey: "",
        errorPrivateKey: "",
    }

    handleValidation = (public_key, private_key) => {
        var a = false
        var b = false

        if (
            //eslint-disable-next-line
            /[#@!$^&~=`{}\[\]";'/<>]/.test(public_key.toString()) ||
            public_key.toString() < 1
        ) {
            this.setState({
                isValidPublicKey: false,
                errorPublicKey: "input-error",
            })
        } else {
            this.setState({
                isValidPublicKey: true,
                errorPublicKey: "",
            })
            a = true
        }

        if (
            //eslint-disable-next-line
            /[#@!$^&~=`{}\[\]";'/<>]/.test(private_key.toString()) ||
            private_key.toString() < 1
        ) {
            this.setState({
                isValidPrivateKey: false,
                errorPrivateKey: "input-error",
            })
        } else {
            this.setState({
                isValidPrivateKey: true,
                errorPrivateKey: "",
            })
            b = true
        }

        if (a && b) {
            this.addServer()
        }
    }

    addServer = () => {
        const {
            i18n: { language },
        } = this.props
        this.setState({ showAlert: true, showSuccess: true })
        fetch(`${API_ENDPOINT}/user/server/add`, {
            method: "POST",
            headers: {
                Authorization: this.props.token,
                "Accept-Language": language,
            },
            body: JSON.stringify({
                public_key: this.state.publicKey,
                private_key: this.state.privateKey,
            }),
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    this.notify(res.error)
                } else {
                    this.notifyS()

                    fetch(`${API_ENDPOINT}/server/names`, {
                        headers: {
                            Authorization: this.props.token,
                            "Accept-Language": language,
                        },
                    })
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                if (res.error === "Invalid token") {
                                    this.props.setToken(null)
                                }
                            } else {
                                this.props.setServerName(res)
                            }
                        })
                }
            })
    }

    notify = error => {
        toast.dismiss()
        toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    notifyS = () => {
        const { t } = this.props
        toast.dismiss()
        toast.success(t("addServer.Server added"), {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
    }

    render() {
        const { t } = this.props
        return (
            <div className='PassForm'>
                <div className='input-field'>
                    <button
                        onClick={() => {
                            this.setState(state => ({
                                showAddServer: !state.showAddServer,
                            }))
                        }}>
                        {t("addServer.Add Server")}
                    </button>

                    {this.state.showAddServer ? (
                        <>
                            <input
                                placeholder={t("addServer.Public key")}
                                type='text'
                                className={
                                    "validate " + this.state.errorPublicKey
                                }
                                value={this.state.publicKey}
                                onChange={e =>
                                    this.setState({ publicKey: e.target.value })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidPublicKey: false,
                                            errorPublicKey: "input-error",
                                        })
                                    } else if (
                                        //eslint-disable-next-line
                                        /[#@!$^&~=`{}\[\]";'/<>]/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidPublicKey: false,
                                            errorPublicKey: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidPublicKey: true,
                                            errorPublicKey: "",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorPublicKey !== "") {
                                        this.setState({
                                            errorPublicKey: "",
                                            isValidPublicKey: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.publicKey,
                                            this.state.privateKey
                                        )
                                    }
                                }}
                            />
                            {this.state.isValidPublicKey ? null : (
                                <p>{t("addServer.validationError")}</p>
                            )}
                            <input
                                placeholder={t("addServer.Private key")}
                                type='text'
                                className={
                                    "validate " + this.state.errorPrivateKey
                                }
                                value={this.state.privateKey}
                                onChange={e =>
                                    this.setState({
                                        privateKey: e.target.value,
                                    })
                                }
                                onBlur={e => {
                                    if (e.target.value.length < 1) {
                                        this.setState({
                                            isValidPrivateKey: false,
                                            errorPrivateKey: "input-error",
                                        })
                                    } else if (
                                        //eslint-disable-next-line
                                        /[#@!$^&~=`{}\[\]";'/<>]/.test(
                                            e.target.value.toString()
                                        )
                                    ) {
                                        this.setState({
                                            isValidPrivateKey: false,
                                            errorPrivateKey: "input-error",
                                        })
                                    } else {
                                        this.setState({
                                            isValidPrivateKey: true,
                                            errorPrivateKey: "",
                                        })
                                    }
                                }}
                                onFocus={() => {
                                    if (this.state.errorPrivateKey !== "") {
                                        this.setState({
                                            errorPrivateKey: "",
                                            isValidPrivateKey: true,
                                        })
                                    }
                                }}
                                onKeyPress={event => {
                                    if (event.key === "Enter") {
                                        this.handleValidation(
                                            this.state.publicKey,
                                            this.state.privateKey
                                        )
                                    }
                                }}
                            />

                            {this.state.isValidPrivateKey ? null : (
                                <p>{t("addServer.validationError")}</p>
                            )}

                            <button
                                disabled={
                                    this.state.publicKey === "" ||
                                    this.state.privateKey === "" ||
                                    this.state.isValidPublicKey === false ||
                                    this.state.isValidPrivateKey === false
                                }
                                onClick={() => {
                                    this.addServer()
                                }}>
                                {t("addServer.Confirm")}
                            </button>
                        </>
                    ) : null}
                </div>
            </div>
        )
    }
}

export default withTranslation()(AddServer)
