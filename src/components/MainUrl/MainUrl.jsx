// import React from "react"
// import { Redirect } from "react-router-dom"
// import VisualConfig from "../VisualConfig/VisualConfig"
// import UrlMain from "../UrlMain//UrlMain"
// import Footer from "../Footer/Footer"
// import Header from "../Header/Header"
// import "./MainUrl.sass"

// class MainUrl extends React.Component {
//     state = {}
//     componentDidMount() {
//         document.title = "Url Monitoring"
//     }
//     render() {

//         if (!this.props.token) {
//             return <Redirect to='/login' exact />
//         }
//         return (
//             <div className='header-main'>
//                 <div className='header-user'>
//                     <Header
//                         token={this.props.token}
//                         setToken={this.props.setToken}
//                     />
//                 </div>
//                 <div className='header-table'>
//                     <UrlMain
//                         token={this.props.token}
//                         setToken={this.props.setToken}
//                     />
//                 </div>
//                 <VisualConfig />
//                 <Footer />
//             </div>
//         )
//     }
// }

// export default MainUrl
