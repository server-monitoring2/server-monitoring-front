import React from "react"
import "./NotFound.sass"
import space from "./img/pepega.png"

class NotFound extends React.Component {
    render() {
        document.title = "Error 404"
        return (
            <div className='NotFound'>
                <div className='left'>
                    <img src={space} alt='Error' />
                </div>
                <div className='header'>
                    <h1>404</h1>
                    <h2>Page not found</h2>
                    <h3>Sorry, the page you are looking for can`t be found</h3>
                    <p>
                        The link you clicked may be broken or the page may have
                        been removed.
                    </p>
                </div>
            </div>
        )
    }
}

export default NotFound
